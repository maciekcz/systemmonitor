=======================
systemmonitor
=======================

Installation
===============
You can install Systemmonitor via Python Package Index ::

    $ pip install systemmonitor

Run server apps on your servers for monitoring them
===================================================
Think up unique secret key for each server and run systemmonitor server application(s) ::

    $ systemmonitor-run-server --port=5002 --host=0.0.0.0 --secret-key=my_secret_token_change_me


You can also run the script (periodically every 15 minutes) for calculating IO reads and writes ::

    $ systemmonitor-update-server-status

Configure your client application
=================================

Run this command to generate Access Token for your client application to have an access to the server webservice::

    $ systemmonitor-generate-token my_username my_secret_token_change_me


Prepare settings file `systemmonitor.yaml` for your client app (to read your servers metrics)
and optionally set up alerts (to be notified if the metrics values are too high)::

  servers:
  - name: "MyLocalServer"
    access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZGVudGl0eSI6Im15X3VzZXJuYW1lIiwidHlwZSI6ImFjY2VzcyIsInVzZXJfY2xhaW1zIjp7fX0.NQqqgb32D4CLhrWu1uXSvXrzrRNq8HA8MRGA--LMBgo"
    url: "http://localhost:5002"

    alerts:
      # set the maximum allowed value for each metric
      memory: 65 # virtual memory usage percent
      load_avg: 1.7 # load avarage in 15 minutes
      cpu: 60 # cpu usage percent
      io:
        - 1000 # KB reads per second
        - 1000 # KB writes per second
      disks:
        - mount_point: /
          limit: 70 # disk usage percent
        - mount_point: /home
          limit: 70 # disk usage percent

  notifications:
    from: 'myusername@myclient-server.com'
    to: 'myusername@myemail.com'

Run servers checker. It's good idea to run it periodically every 15 minutes. ::

    $ systemmonitor-check-servers systemmonitor.yaml

Check your webpage URLs
=======================

You can also check if for example some webpage returns valid response.
First add to your settings file an `urls` key ::

  urls:
    - url: "http://maciek.cz/"
      name: "My web page"
      conditions:
        - condition: "'Python' in content.decode('utf-8')"
          name: "`Python` in web page content"

  notifications:
    to: 'myusername@myserver.com'

And run a command ::

  $ systemmonitor-check-urls systemmonitor.yaml

You can also check both of them - servers and URLs using one command ::

  $ systemmonitor-check-all systemmonitor.yaml


