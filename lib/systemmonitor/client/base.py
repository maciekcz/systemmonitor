import argparse
import logging

import yaml

from .reader import ServerReader
from .configparser import get_alert_config
from .checkers import check_metric_limit, METRICS
from ..notifications import send_error_message

logger = logging.getLogger(__name__)


def check_servers(config=None):

    if config is None:
        parser = argparse.ArgumentParser()
        parser.add_argument('config_file', help='config YAML file')
        args = parser.parse_args()

        with open(args.config_file, "r") as stream:
            config = yaml.load(stream, Loader=yaml.BaseLoader)

    errors_to_send = []
    for server_config in config['servers']:
        server_reader = ServerReader(server_config)
        try:
            server_response = server_reader.read()
        except Exception as e:
            logger.error("[{}]: {}".format(server_config['name'], e))
            errors_to_send.append((
                server_config,
                {
                    'ok': False,
                    'alerts': ['read'],
                    'details': {'read': "{}".format(e)}
                 }
            ))
        else:
            server_metrics = server_response['data']
            status = {
                'ok': True,
                'alerts': [],
                'details': {
                    'uptime': server_metrics['uptime'] if 'uptime' in server_metrics else None,
                    'read': server_response['read_time']
                }
            }

            alert_config = get_alert_config(server_config)

            for metric_name in METRICS:
                try:
                    rs = check_metric_limit(metric_name, server_metrics, alert_config)
                except KeyError as e:
                    status['details'][metric_name] = 'No response data'
                else:
                    status['details'][metric_name] = rs['details']
                    if not rs['ok']:
                        status['ok'] = False
                        status['alerts'].append(metric_name)

            if not status['ok']:
                errors_to_send.append((server_config, status))
                logger.error("[{}] {}".format(server_config['name'], status))
            else:
                logger.info("[{}] {}".format(server_config['name'], status))

    if errors_to_send:
        send_error_message(config, errors_to_send)
