from datetime import datetime

METRICS = ('memory', 'load_avg', 'cpu', 'disks', 'io')


def check_metric_limit(metric, server_data, alert_config):
    rs = globals()["check_{}".format(metric)](
        server_data[metric],
        alert_config[metric]
    )
    return rs


def _get_used_memory(value):
    used_mem = round((1.0 - float(value[7] + value[8] + value[4]) / value[0]) * 100, 1)
    return used_mem


def check_memory(value, critical_value):
    rs = {'ok': True}
    used_mem = _get_used_memory(value)
    rs['used_memory'] = used_mem
    rs['critical_value'] = critical_value
    if critical_value is not None:
        if used_mem > critical_value:
            rs['ok'] = False

    rs['details'] = (used_mem, critical_value)
    return rs


def check_load_avg(value, critical_value=None):
    rs = {'ok': True}
    if critical_value is not None and value[1] > critical_value:
        rs['ok'] = False

    rs['details'] = (value[1], critical_value)
    return rs


def check_cpu(value, critical_value=None):
    rs = {'ok': True}
    if critical_value is not None and value > critical_value:
        rs['ok'] = False
    rs['details'] = (value, critical_value)
    return rs


def check_disks(value, critical_values):
    rs = {'ok': True, 'details': []}
    try:
        if critical_values:
            warning_dict = {a['mount_point']: float(a['limit']) for a in critical_values}
            for item in value:
                if item[0] in warning_dict:
                    usage_limit = warning_dict[item[0]]
                    real_usage = item[1][3]
                    rs['details'].append([item[0], real_usage, usage_limit])
                    if real_usage > usage_limit:
                        rs['ok'] = False
        else:
            for item in value:
                real_usage = item[1][3]
                rs['details'].append([item[0], real_usage, None])
    except KeyError:
        pass
    return rs


def check_io(disk_io_diff, critical_values):
    rs = {'ok': True, 'details': []}
    if disk_io_diff is not None:
        valid_time_range = True
        if critical_values['hours_ranges']:
            now = datetime.now()
            found_valid_range = False
            for time_range in critical_values['hours_ranges']:
                if now > time_range[0] and now < time_range[1]:
                    found_valid_range = True
            if not found_valid_range:
                valid_time_range = False

        date_time_obj_old = datetime.strptime(disk_io_diff[0][0], '%Y-%m-%d %H:%M:%S.%f')
        date_time_obj_current = datetime.strptime(disk_io_diff[1][0], '%Y-%m-%d %H:%M:%S.%f')
        if (datetime.now() - date_time_obj_current).total_seconds() > 60 * 60:
            rs['details'].append('Expired')
        else:
            total_seconds = (date_time_obj_current - date_time_obj_old).total_seconds()
            hours_ranges = None
            if critical_values['hours_ranges']:
                hours_ranges = ["{}-{}".format(
                    item[0].strftime("%H:%M"), item[1].strftime("%H:%M")) for item in critical_values['hours_ranges']
                ]
            rs['details'] = (
                (round((disk_io_diff[1][1] - disk_io_diff[0][1]) / total_seconds / 1024, 2),
                 round((disk_io_diff[1][2] - disk_io_diff[0][2]) / total_seconds / 1024, 2)),
                [critical_values['read_limit'], critical_values['write_limit'], hours_ranges]
            )

            try:
                if valid_time_range and (
                        rs['details'][0][0] > critical_values['read_limit'] or
                        rs['details'][0][1] > critical_values['write_limit']
                ):
                    rs['ok'] = False
            except TypeError:
                pass
    return rs




