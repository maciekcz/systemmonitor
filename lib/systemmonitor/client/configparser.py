from datetime import datetime


def parse_hours_ranges(data):
    now = datetime.now()
    rs = []
    for row in data:
        start, end = row.split(',')
        start_hour, start_minute = start.split(':')
        end_hour, end_minute = end.split(':')
        start_datetime = datetime(now.year, now.month, now.day, int(start_hour), int(start_minute))
        end_datetime = datetime(now.year, now.month, now.day, int(end_hour), int(end_minute))
        rs.append((start_datetime, end_datetime))

    return rs


def get_alert_config(server_settings):
    rs = {}
    try:
        alert_limits = server_settings['alerts']
    except KeyError:
        alert_limits = {}
    try:
        rs['memory'] = float(alert_limits['memory'])
    except KeyError:
        rs['memory'] = None
    try:
        rs['load_avg'] = float(alert_limits['load_avg'])
    except KeyError:
        rs['load_avg'] = None
    try:
        rs['cpu'] = float(alert_limits['cpu'])
    except (KeyError, TypeError):
        rs['cpu'] = None
    try:
        rs['disks'] = alert_limits['disks']
    except KeyError:
        rs['disks'] = None
    try:
        hours_ranges = parse_hours_ranges(alert_limits['io'][2]['hours_ranges'])
    except (KeyError, IndexError):
        hours_ranges = None
    try:
        rs['io'] = {'read_limit': float(alert_limits['io'][0]), 'write_limit': float(alert_limits['io'][1]),
                    'hours_ranges': hours_ranges}

    except KeyError:
        rs['io'] = None
    return rs
