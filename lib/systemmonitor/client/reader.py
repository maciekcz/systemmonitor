import json

import requests

DEFAULT_READ_TIMEOUT = 10


class ServerReader:

    def __init__(self, server_config):
        self.server_settings = server_config

    def read(self):
        read_timeout = DEFAULT_READ_TIMEOUT
        if 'read_timeout' in self.server_settings:
            read_timeout = int(self.server_settings['read_timeout'])
        url = self.server_settings['url']

        headers = {'Authorization': 'Bearer {}'.format(self.server_settings['access_token'])}
        doc = requests.get(url, timeout=read_timeout, headers=headers)
        if doc.status_code != 200:
            raise Exception(
                'Status code is {} and content: {}'.format(doc.status_code, doc.content[:100] if doc.content else '')
            )
        return {'read_time': doc.elapsed.total_seconds(), 'data': json.loads(doc.content.decode('utf-8'))}

