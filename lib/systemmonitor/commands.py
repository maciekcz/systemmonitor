import argparse
import logging
from datetime import datetime

import jwt
import yaml
from .client.base import check_servers
from .urlschecker import check_urls as check_urls_data

logger_handler = logging.StreamHandler()  # Handler for the logger
logger_handler.setFormatter(logging.Formatter('%(levelname)s [%(name)s:%(lineno)s] %(message)s'))
logger = logging.getLogger()
logger.addHandler(logger_handler)
logger.setLevel(logging.INFO)


def __read_yaml_settings():
    logger.info("############### Reading YAML settings ################ {}".format(datetime.now()))
    parser = argparse.ArgumentParser()
    parser.add_argument('config_file', help='config YAML file')
    args = parser.parse_args()

    with open(args.config_file, "r") as stream:
        settings = yaml.load(stream, Loader=yaml.BaseLoader)
    return settings


def check_all():
    settings = __read_yaml_settings()
    check_servers(settings)
    check_urls_data(settings)


def check_machines():
    settings = __read_yaml_settings()
    check_servers(settings)


def check_urls():
    settings = __read_yaml_settings()
    check_urls_data(settings)


def generate_access_token():
    parser = argparse.ArgumentParser()
    parser.add_argument('username', help='User name')
    parser.add_argument('secret-key', help='Host name', default='')
    args = parser.parse_args()
    secret_key = args.__getattribute__('secret-key')
    payload = {
        'identity': args.username,
        'type': u'access',
        'user_claims': {}
    }
    access_token = jwt.encode(payload, secret_key)
    print(access_token)
