import logging
import smtplib
from email.mime.text import MIMEText

logger = logging.getLogger(__name__)


def send_error_message(settings_dict: dict, errors: list):
    if 'notifications' in settings_dict and errors:
        settings = settings_dict['notifications']
        txt = 'Errors registered on pages below:\n'
        for error in errors:
            txt += "{}\n{}\n".format(error[0]['name'], error[1])
        logger.info(txt)
        s = smtplib.SMTP('localhost')
        msg = MIMEText(txt)
        msg['Subject'] = '[SystemMonitor ALERT] Errors registered on pages'
        try:
            msg['From'] = settings['from']
        except KeyError:
            msg['From'] = 'systemmonitor@localhost'
        msg['To'] = settings['to']
        s.send_message(msg)
        s.quit()
