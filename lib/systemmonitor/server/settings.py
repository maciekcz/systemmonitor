import os

import pkg_resources

SERVER_VERSION = pkg_resources.get_distribution("systemmonitor").version

SERVER_STATUS_FILE_NAME = 'serverstatus.txt'

HOMEDIR = os.environ['HOME']

SERVER_STATUS_FILE_DIRECTORY = os.path.join(HOMEDIR, ".systemmonitor")
SERVER_STATUS_FILE_PATH = os.path.join(SERVER_STATUS_FILE_DIRECTORY, SERVER_STATUS_FILE_NAME)
