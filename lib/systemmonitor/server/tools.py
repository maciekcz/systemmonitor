import os
from datetime import datetime

import psutil

from .settings import SERVER_STATUS_FILE_DIRECTORY, SERVER_STATUS_FILE_PATH


def _create_status_file():
    try:
        os.mkdir(SERVER_STATUS_FILE_DIRECTORY)
    except FileExistsError:
        pass
    with open(SERVER_STATUS_FILE_PATH, 'a') as f:
        pass


def update_server_status():
    _create_status_file()
    stat = psutil.disk_io_counters()
    with open(SERVER_STATUS_FILE_PATH, 'r') as f:
        lines = f.readlines()
        try:
            last_result = lines[len(lines) - 1]
        except IndexError:
            last_result = None
    with open(SERVER_STATUS_FILE_PATH, 'w') as f:
        if last_result is not None:
            f.write(last_result)
        f.write('{} {} {}\n'.format(datetime.now(), stat[2], stat[3]))