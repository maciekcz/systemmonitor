import argparse
from datetime import datetime

import pytz
import os
import psutil
import json
from flask import Flask
from flask_jwt_extended import JWTManager, jwt_required

from .settings import SERVER_STATUS_FILE_PATH, SERVER_VERSION

app = Flask(__name__)

try:
    secret_key = os.environ['SYSTEMMONITOR_SECRET']
except KeyError:
    secret_key = None


app.config['JWT_SECRET_KEY'] = secret_key
jwt_app = JWTManager(app)


@app.route("/")
@jwt_required
def monitorserver():

    la = os.getloadavg()
    rs = {'load_avg': la, 'disks': []}
    for disk_info in psutil.disk_partitions():
        path = disk_info.mountpoint
        try:
            usage = psutil.disk_usage(path)
        except PermissionError:
            pass
        else:
            rs['disks'].append((path, tuple(usage)))
    try:
        with open(SERVER_STATUS_FILE_PATH, 'r') as f:
            lines = f.readlines()
    except (TypeError, FileNotFoundError):
        lines = []

    if len(lines) == 2:
        iostat = []
        for line in lines:
            line_data = line.split(' ')
            iostat.append((line_data[0] + ' ' + line_data[1], int(line_data[2]), int(line_data[3])))
        rs['io'] = iostat

    rs['cpu'] = psutil.cpu_percent()
    rs['uptime'] = (datetime.now() - datetime.fromtimestamp(psutil.boot_time())).days
    rs['memory'] = tuple(psutil.virtual_memory())
    rs['timestamp'] = str(datetime.utcnow().replace(tzinfo=pytz.utc))
    rs['version'] = SERVER_VERSION
    return json.dumps(rs)


def runserver():
    global jwt_app
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', help='Port number')
    parser.add_argument('--host', help='Host name')
    parser.add_argument('--secret-key', help='Host name')

    args = parser.parse_args()
    if args.secret_key:
        secret_key = args.secret_key
        app.config['JWT_SECRET_KEY'] = secret_key
        jwt_app = JWTManager(app)
    app.run(port=args.port, host=args.host)


if __name__ == "__main__":
    """
    Run example:
    $ SYSTEMMONITOR_SECRET=my-super-secret-key systemmonitor-machine-monitor
    """
    runserver()
