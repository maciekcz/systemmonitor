from unittest import TestCase

from systemmonitor.client.checkers import check_memory, check_cpu


class ClientCheckers(TestCase):
    def test_check_memory_ok(self):
        v = [16875905024, 14487953408, 14.2, 1897746432, 836988928, 10606206976, 4483919872, 1011105792, 13130063872,
             146006016, 793124864]
        cv = 65.0
        rs = check_memory(v, cv)
        self.assertTrue(rs, {'ok': True, 'used_memory': 11.2, 'critical_value': 65.0, 'details': (11.2, 65.0)})

    def test_check_memory_alert(self):
        v = [16875905024, 14487953408, 14.2, 1897746432, 836988928, 10606206976, 4483919872, 101, 13,
             146006016, 793124864]
        cv = 65.0
        rs = check_memory(v, cv)
        self.assertFalse(rs['ok'])

    def test_check_cpu(self):
        rs = check_cpu(12, 44)
        self.assertTrue(rs['ok'])
        self.assertTrue(rs['details'] == (12, 44))

    def test_check_cpu_alert(self):
        rs = check_cpu(47, 44)
        self.assertFalse(rs['ok'])
        self.assertTrue(rs['details'] == (47, 44))
