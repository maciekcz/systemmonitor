from unittest import TestCase
from systemmonitor.server.webservice import app

TEST_SECRET_KEY = 'my_secret_token_change_me'
TEST_ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZGVudGl0eSI6Im15X3VzZXJuYW1lIiwidHlwZSI6ImFjY2VzcyIsIn" \
                    "VzZXJfY2xhaW1zIjp7fX0.NQqqgb32D4CLhrWu1uXSvXrzrRNq8HA8MRGA--LMBgo"


class TestServer(TestCase):
    def setUp(self) -> None:
        app.config['TESTING'] = True
        app.config['JWT_SECRET_KEY'] = TEST_SECRET_KEY
        self.app = app.test_client()

    def test_auth_ok(self):
        response = self.app.get('/', headers={'Authorization': 'Bearer {}'.format(TEST_ACCESS_TOKEN)})
        self.assertEqual(response.status_code, 200)

    def test_auth_error(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 401)
        response = self.app.get('/', headers={'Authorization': 'Bearer {}'.format("invalid access token")})
        self.assertEqual(response.status_code, 422)
