import yaml
import argparse
import logging
import socket
import json

import requests
from datetime import datetime

from .notifications import send_error_message

logger = logging.getLogger(__name__)


class ValidationError(Exception):
    pass


def check(url, conditions=None, timeout=None):
    timeout = 5 if timeout is None else timeout
    try:
        response = requests.get(url, timeout=timeout)
    except (socket.timeout, requests.exceptions.ConnectTimeout):
        raise ValidationError('Timeout error')
    except requests.exceptions.SSLError:
        raise ValidationError('SSL certificate error')
    except Exception as e:
        raise ValidationError("Unhandled error: {}".format(e))

    if response.status_code != 200:
        raise ValidationError('HTTP status code not equals 200')
    if conditions:
        content = response.content
        for condition in conditions:
            condition_formula = condition
            condition_name = condition
            if isinstance(condition, dict):
                condition_formula = condition['condition']
                condition_name = condition['name']
            
            try:
                if not eval(condition_formula):
                    raise ValidationError('Condition `{}` not passed'.format(condition_name))
            except Exception as e:
                if not isinstance(e, ValidationError):
                    raise ValidationError('Condition `{}` thrown exception: {}'.format(condition_name, e))
                else:
                    raise

    return True


def short_url_version_if_needed(url):
    rs = url
    if len(url) > 60:
        rs = url[:60] + '...'
    return rs


def check_urls(settings=None):
    if settings is None:
        parser = argparse.ArgumentParser()
        parser.add_argument('config_file', help='config YAML file')
        args = parser.parse_args()
        with open(args.config_file, "r") as stream:
            settings = yaml.load(stream, Loader=yaml.SafeLoader)

    logger.debug(settings)
    errors_to_send = []
    try:
        urls = settings['urls']
    except KeyError:
        urls = []
    for row in urls:
        url = row['url']
        url_name = row.get('name', None)
        conditions = row['conditions'] if 'conditions' in row else None
        timeout = int(row['timeout']) if 'timeout' in row else None
           
        try:
            check(url, conditions=conditions, timeout=timeout)
        except ValidationError as e:
            logger.error("%s: %s", url_name if url_name is not None else short_url_version_if_needed(url), e)
            errors_to_send.append((row, e))
        else:
            logger.info("%s: %s", url_name if url_name is not None else short_url_version_if_needed(url), 'OK')
    if errors_to_send:
        send_error_message(settings, errors_to_send)
