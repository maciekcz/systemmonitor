# -*- coding: utf-8 -*-
import sys

try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

PACKAGE = 'systemmonitor'
VERSION = '0.1.0-dev'

if sys.version_info < (3, 2):
    raise SystemExit("Python 3.2 or later is required  for %s %s" % (PACKAGE,
                                                                     VERSION))

setup(
    name=PACKAGE,
    version=VERSION,
    description='',
    long_description=open("README.rst").read() + open("CHANGES").read(),
    keywords='',
    author="Maciej Czerwinski",

    author_email="maciekcz2@gmail.com",
    maintainer="Maciej Czerwinski",
    maintainer_email="maciekcz2@gmail.com",
    url='',
    packages=find_packages('lib'),
    package_dir={'': 'lib'},
    package_data={'': []},
    namespace_packages=['systemmonitor'],
    entry_points={
        'console_scripts': [
            'systemmonitor-run-server=systemmonitor.server.webservice:runserver',
            'systemmonitor-generate-token=systemmonitor.commands:generate_access_token',
            'systemmonitor-check-all=systemmonitor.commands:check_all',
            'systemmonitor-check-servers=systemmonitor.commands:check_machines',
            'systemmonitor-check-urls=systemmonitor.commands:check_urls',
            'systemmonitor-update-server-status=systemmonitor.server.tools:update_server_status',
        ]
    },
    install_requires=[
        'setuptools',
        'pyyaml',
        'psutil',
        'pytz',
        'flask==1.0.2',
        'flask-jwt-extended',
        'requests',
        'nose',
        'mock',
        'requests_mock',
    ],
    zip_safe=False,
    platforms='All',
    test_suite='nose.collector',
)

